package com.artemis.fsm.providers;

import com.artemis.Component;
import com.artemis.fsm.IComponentProvider;
import com.artemis.utils.reflect.ClassReflection;
import com.artemis.utils.reflect.ReflectionException;

public class ComponentSingletonProvider implements IComponentProvider {
  private Class<? extends Component> componentType;
  private Component instance;

  public ComponentSingletonProvider(Class<? extends Component> type) {
    this.componentType = type;
  }

  @Override
  public Component getComponent() throws IllegalAccessException, ReflectionException {
    if (instance == null) {
      instance = ClassReflection.newInstance(componentType);
    }
    return instance;
  }

  @Override
  public String identifier() {
    return ClassReflection.getSimpleName(componentType);
  }
}
