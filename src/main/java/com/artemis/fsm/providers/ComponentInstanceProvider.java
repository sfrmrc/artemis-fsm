package com.artemis.fsm.providers;

import com.artemis.Component;
import com.artemis.fsm.IComponentProvider;
import com.artemis.utils.reflect.ClassReflection;
import com.artemis.utils.reflect.ReflectionException;

public class ComponentInstanceProvider implements IComponentProvider {
  private final Component component;

  public ComponentInstanceProvider(Component component) {
    this.component = component;
  }

  @Override
  public Component getComponent() throws IllegalAccessException, ReflectionException {
    return component;
  }

  @Override
  public String identifier() {
    return ClassReflection.getSimpleName(component.getClass());
  }
}
