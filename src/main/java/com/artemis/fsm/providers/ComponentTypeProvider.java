package com.artemis.fsm.providers;

import com.artemis.Component;
import com.artemis.fsm.IComponentProvider;
import com.artemis.utils.reflect.ClassReflection;
import com.artemis.utils.reflect.ReflectionException;

public class ComponentTypeProvider implements IComponentProvider {
  private Class<? extends Component> componentType;

  public ComponentTypeProvider(Class<? extends Component> type) {
    this.componentType = type;
  }

  @Override
  public Component getComponent() throws IllegalAccessException, ReflectionException {
    return ClassReflection.newInstance(componentType);
  }

  @Override
  public String identifier() {
    return ClassReflection.getSimpleName(componentType);
  }
}
