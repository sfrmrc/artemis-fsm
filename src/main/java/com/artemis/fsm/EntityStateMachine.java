package com.artemis.fsm;

import java.util.HashMap;
import java.util.Map;

import com.artemis.Component;
import com.artemis.Entity;
import com.artemis.EntityEdit;
import com.artemis.utils.reflect.ReflectionException;

/**
 * This is a state machine for an entity. The state machine manages a set of states, each of which
 * has a set of component providers. When the state machine changes the state, it removes components
 * associated with the previous state and adds components associated with the new state.
 */
public class EntityStateMachine<E extends Enum<E>> {
  private HashMap<E, EntityState<E>> states;
  private EntityState<E> currentState;
  private Entity entity;

  public EntityStateMachine(Entity entity) {
    this.entity = entity;
    states = new HashMap<E, EntityState<E>>();
  }

  public final E currentState() {
    return currentState.name;
  }

  /**
   * Add a state to this state machine.
   * 
   * @param name The name of this state - used to identify it later in the changeState method call.
   * @param state The state.
   * @return This state machine, so methods can be chained.
   */
  public EntityStateMachine<E> addState(E name, EntityState<E> state) {
    states.put(name, state);
    return this;
  }

  /**
   * Create a new state in this state machine.
   * 
   * @param name The name of the new state - used to identify it later in the changeState method
   *        call.
   * @return The new EntityState object that is the state. This will need to be configured with the
   *         appropriate component providers.
   */
  public EntityState<E> createState(E name) {
    EntityState<E> state = new EntityState<E>(name);
    addState(name, state);
    return state;
  }

  /**
   * Change to a new state. The components from the old state will be removed and the components for
   * the new state will be added.
   * 
   * @param name The name of the state to change to.
   */
  public void changeState(E name) {
    EntityState<E> newState = states.get(name);
    EntityEdit entityEdit = entity.edit();

    /* Ensure state exists */
    if (newState == null) {
      throw new RuntimeException("Entity State " + name + " does not exist");
    }
    /* Nothing to do if already current state */
    if (newState == currentState) {
      return;
    }

    HashMap<Class<? extends Component>, IComponentProvider> toAdd;
    /* If there is a current state, query which Components to add/remove */
    if (currentState != null) {
      /* Add all */
      toAdd = new HashMap<Class<? extends Component>, IComponentProvider>(newState.providers);

      /* Exclude */
      for (Map.Entry<Class<? extends Component>, IComponentProvider> entry : currentState.providers
          .entrySet()) {
        //IComponentProvider other = toAdd.get(entry.getKey());

        /* If the current state already has the requested Component, do not add it */
        // if (other != null && other.identifier().equals(entry.getValue().identifier())) {
        // toAdd.remove(entry.getKey());
        // }
        // Otherwise ensure that the entity does not have the Component. It will be re-added
        // else {
        entityEdit.remove(entry.getKey());
        // }
      }
    }
    /* Else, no processing needed */
    else {
      toAdd = newState.providers;
    }

    /* Go ahead and add new Components */
    for (Map.Entry<Class<? extends Component>, IComponentProvider> entry : toAdd.entrySet()) {
      /* Java needs this... */
      try {
        entityEdit
            .add(entry.getValue().getComponent()/* , ComponentType.getTypeFor(entry.getKey()) */);
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (ReflectionException e) {
        e.printStackTrace();
      }
    }

    currentState = newState;
  }
}
