package com.artemis.fsm;

import java.util.HashMap;

import com.artemis.Component;

public class EntityState<E> {
  E name;
  HashMap<Class<? extends Component>, IComponentProvider> providers =
      new HashMap<Class<? extends Component>, IComponentProvider>();

  public EntityState(E name) {
    this.name = name;
  }

  /**
   * Add a new ComponentMapping to this state. The mapping is a utility class that is used to map a
   * component type to the provider that provides the component.
   * 
   * @param type The type of component to be mapped
   * @return The component mapping to use when setting the provider for the component
   */
  public StateComponentMapping add(Class<? extends Component> type) {
    return new StateComponentMapping(this, type);
  }

  /**
   * Get the ComponentProvider for a particular component type.
   * 
   * @param type The type of component to get the provider for
   * @return The ComponentProvider
   */
  public IComponentProvider get(Class<? extends Component> type) {
    return providers.get(type);
  }

  /**
   * To determine whether this state has a provider for a specific component type.
   * 
   * @param type The type of component to look for a provider for
   * @return true if there is a provider for the given type, false otherwise
   */
  public Boolean has(Class<? extends Component> type) {
    return providers.get(type) != null;
  }
}
