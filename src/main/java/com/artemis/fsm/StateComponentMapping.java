package com.artemis.fsm;

import com.artemis.Component;
import com.artemis.fsm.providers.ComponentInstanceProvider;
import com.artemis.fsm.providers.ComponentSingletonProvider;
import com.artemis.fsm.providers.ComponentTypeProvider;

public class StateComponentMapping {
  private final EntityState creatingState;
  private final Class<? extends Component> componentType;
  private IComponentProvider provider;

  /**
   * Used internally, the constructor creates a component mapping. The constructor creates a
   * ComponentTypeProvider as the default mapping, which will be replaced by more specific mappings
   * if other methods are called.
   * 
   * @param creatingState The EntityState that the mapping will belong to
   * @param type The component type for the mapping
   */
  public StateComponentMapping(EntityState creatingState, Class<? extends Component> type) {
    this.creatingState = creatingState;
    this.componentType = type;
    withType(type);
  }

  /**
   * Creates a mapping for the component type to a specific component instance. A
   * ComponentInstanceProvider is used for the mapping.
   * 
   * @param component The component instance to use for the mapping
   * @return This ComponentMapping, so more modifications can be applied
   */
  public StateComponentMapping withInstance(Component component) {
    setProvider(new ComponentInstanceProvider(component));
    return this;
  }

  /**
   * Creates a mapping for the component type to new instances of the provided type. The type should
   * be the same as or extend the type for this mapping. A ComponentTypeProvider is used for the
   * mapping.
   * 
   * @param type The type of components to be created by this mapping
   * @return This ComponentMapping, so more modifications can be applied
   */
  public StateComponentMapping withType(Class<? extends Component> type) {
    setProvider(new ComponentTypeProvider(type));
    return this;
  }

  /**
   * Creates a mapping for the component type to a single instance of the provided type. The
   * instance is not created until it is first requested. The type should be the same as or extend
   * the type for this mapping. A ComponentSingletonProvider is used for the mapping.
   * 
   * @param type The type of the single instance to be created. If omitted, the type of the mapping
   *        is used.
   * @return This ComponentMapping, so more modifications can be applied
   */
  public StateComponentMapping withSingleton(Class<? extends Component> type) {
    setProvider(new ComponentSingletonProvider(type));
    return this;
  }

  public StateComponentMapping withSingleton() {
    return withSingleton(componentType);
  }

  /**
   * Creates a mapping for the component type to any ComponentProvider.
   * 
   * @param provider The component provider to use.
   * @return This ComponentMapping, so more modifications can be applied.
   */
  public StateComponentMapping withProvider(IComponentProvider provider) {
    setProvider(provider);
    return this;
  }

  private void setProvider(IComponentProvider provider) {
    this.provider = provider;
    creatingState.providers.put(componentType, provider);
  }

  /**
   * Maps through to the add method of the EntityState that this mapping belongs to so that a fluent
   * interface can be used when configuring entity states.
   * 
   * @param type The type of component to add a mapping to the state for
   * @return The new ComponentMapping for that type
   */
  public StateComponentMapping add(Class<? extends Component> type) {
    return creatingState.add(type);
  }
}
