## Artemis Finite State Machine

Use Finite State Machine inside [Artemis Framework](https://github.com/junkdog/artemis-odb)

## Highlights

* Finite State Machine
* Full GWT support

For more details, see [Original Project](https://github.com/felixSchl/artemis)